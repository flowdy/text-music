title: Anfängerstueck 1
composer: Florian L. Heß
license: CC by-nd 4.0
version: Entwurf 10 - 29.6.2018

stage: # rel. strength left|right # rel. volume # instrument
    piano: 1|1 0 dev/piano

---
# Measure 1
_meta:
    stress_pattern: 3,2;1,0;1,0
    ticks_per_minute: 220
    lower_stress_bound: 80-86
    upper_stress_bound: 88-92
piano:
    0: # Stimme:  B T A S
       - B4 6   #       r5 # Ped.
       - E3 6   # l3
    2:
       - A4 4   #       r4
    3:
       - Gs4 3  #     r3
    4:
       - B4 2   #       r5 # o. Ped.
       - E4 2   #     r1
       - B3 4   #   l1
       - Gs3 4  # l3
    6:
       - Gs4 2  #     r3
---
# Measure 2
_meta:
    lower_stress_bound: 86-90
    upper_stress_bound: 92-95
piano:
    0: # S | Stm.: B T A S
       - A4 6   #       r4 # Ped.
       - E4 6   #     r1
       - Cs4 6  #   l1
       - Cs3 6  # l5
    2:
       - A4 4   #       r4
    3:
       - E4 3   #     r3
    4: # D
       - Ds4 2  #       r2 # o. Ped.
       - B3 2   #     r1
       - Fs3 4  #   l1
       - Ds3 4  # l4
    6:
       - Fs4 2  #       r4
       - Ds4 2  #     r2
---
# Measune 3
piano:
    0: # Stimme:  B T A S
       - E4 8   #       r1 # Ped.
       - B3 8   #     l1
       - Gs3 8  #   l3
    2:
       - E3 6   # l4
    4:
       - B3 4   #     l1
       - Gs3 4  #   l3
    6:
       - E4 2   #     r1
    7:
       - Gs4 1  #       r3

---
# Measure 4
_meta:
    lower_stress_bound: 93
    upper_stress_bound: 97
piano:
    0: # Stimme:  B T A S
       - B4 6   #       r5 # ern. Ped.
       - Gs4 6  #     r3
       - Ds4 6  #   r1
       - Gs2 6  # l5
    2:
       - B4 4   #     r3
    3:
       - Ds5 3  #       r5
    4: 
       - Ds5 2  #       r5
       - B4 2   #     r3
       - Gs4 2  #   r1
       - Ds3 4  # l1
    6:
       - B4 1   #     r3     # o. Ped.
    7:
       - Gs4 1  #   r1

---
# Measure 5
piano:
    0: # Sp
       - A4 6              # Ped.
       - Fs4 6
       - Cs3 6
    2:
       - A4 4
       - Cs4 4
    3: Fs4 3
    4:
       - Fs4 2
       - Cs4 2
       - Fs3 4
    6: Fs4 2               # o. Ped.

---
# Measure 6
piano:
    0: # D^
       - A4 4              # Ped.
       - Fs4 4
       - Fs3 8
    2: A4 1.5
    4:                     # o. Ped.
       - Ds5 2
       - A4 2
    6: Ds5 1
    7: Fs4 1

---
# Measure 7
piano:
    0:
       - Fs4 6
       - Ds4 6
       - A3 6
    2:
       - A4 4
       - Ds4 4
    4:
       - Gs4 2
       - Ds3 4
    6: Fs4 2

---
# Measure 8
piano:
    0: # T
       - E4 8
       - B3 8
       - Gs3 8
       - E3 8
    4:
       - B3 4
       - Gs3 4
    5: E4 3
    6: Gs4 2
    7: E4 1

---
# Measure 9
piano:
    0: # Dp
       - B4 6
       - Gs4 6
       - Ds4 6
       - Gs3 6
    2: B4 4
    3: Ds5 3
    4: # T
       - Gs5 2
       - Ds5 2
       - B4 2
       - Ds4 4
    6: B4 2

---
# Measure 10
piano:
    0: # S
       - Cs5 5
       - A4 5
       - E4 5
       - A3 5
    2: A4 3
    3: A4 2
    4:
       - A4 1
       - Cs4 4
       - Cs3 4
    5:
       - E4 1
       - Cs4 1
    6: Fs4 1
    7: E4 1

---
# Measure 11
piano:
    0: # T
       - E4 6
       - B3 6
       - Gs3 6
       - E3 6
    2: B3 4
    4:
       - Gs4 2
       - E4 2
       - B3 4
       - B2 4
    6: B4 2

---
# Measure 12
piano:
    0: # Tp
       - Cs4 6
       - Gs3 6
       - Cs3 6
    2: E4 4
    4:
       - Gs4 2
       - Cs4 2
       - E3 4
    6: Cs5 2
    7: E4 1

---
# Measure 13
piano:
    0: # S
       - Cs5 2
       - A4 1.5
       - E4 1.5
       - A3 8
    2:
       - A4 0.75
       - E4 1
    3: A4 1
    4:
       - Cs4 0.5
       - Fs3 1
    5:
       - E4 1
       - Cs4 1
    6: Fs4 1
    7: E4 1

---
# Measure 14
_meta:
    lower_stress_bound: 97
    upper_stress_bound: 100
piano:
    0:
       - Cs5 2
       - A4 2
       - E4 3
       - A2 8
    2: Cs4 1
    3: A4 0.75
    4: A4 1
    5:
       - E4 1
       - Cs4 1
    6: Fs4 1
    7: E4 1

---
# Measure 15
piano:
    0: # D
       - Fs4 10
       - Ds4 10
       - B3 10
       - Fs3 10
    3: Ds4 7
    4: # T
       - E4 8
       - B3 8
       - Gs3 8
       - E3 12

---
# Measure 16
_meta:
    lower_stress_bound: 80
    upper_stress_bound: 87
piano:
    4: B3 1
    5: E4 1
    6: Gs4 1
    7: B4 1

---
# Measure 17
piano:
    0:
    - B4 2
    - Fs4 2
    - Ds4 2
    - B3 4
    2:
    - Fs4 1
    3:
    - Ds4 1
    4:
    - E4 2
    - B3 2
    - Gs3 4
    6:
    - Gs4 2

---
# Measure 18
piano:
    0:
    - A4 2
    - Fs4 2
    - Cs4 2
    - Fs3 4
    2:
    - Fs4 1
    3:
    - Cs4 1
    4:
    - Fs4 2
    - D4 2
    - B3 4
    6:
    - Fs4 1
    7:
    - B4 1

---
# Measure 19
piano:
    0:
    - Gs4 2
    - E4 2
    - Cs4 2
    - Gs3 4
    2:
    - E4 1
    3:
    - Cs4 1
    4:
    - E4 2
    - Gs3 2
    - Cs3 4
    6:
    - E3 2
 
---
# Measure 20
piano:
    0:
    - E4 2
    - Cs4 2
    - Gs3 2
    - Gs2 8
    2:
    - Cs4 1
    3:
    - Gs3 1
    4:
    - Cs4 2
    - E3 2
    6:
    - Cs4 1
    7:
    - E4 1

---
# Measure 21
piano:
    0:
    - Gs4 4
    - E4 4
    - Cs4 4
    - Gs2 4
    2:
    - Gs4 2
    3:
    - E4 2
    4:
    - Cs4 1
    - Cs3 1
    5:
    - Gs3 1
    6:
    - Cs4 1
    - E3 2
    7:
    - E4 1

---
# Measure 22
_meta:
    lower_stress_bound: 80-88
    upper_stress_bound: 87-95
piano:
    0:
    - Fs4 4
    - Cs4 4
    2:
    - A3 2
    4:
    - Fs4 2
    - Fs3 4
    6:
    - A4 2

---
# Measure 23
_meta:
    lower_stress_bound: 85-95
    upper_stress_bound: 95-97
piano:
    0:
    - B4 4
    - Fs4 4
    - Fs3 4
    - Ds3 4
    2:
    - B4 2
    3:
    - Fs4 1
    4:
    - Cs4 4
    - Gs3 4
    - E3 4

---
# Measure 24
_meta:
    lower_stress_bound: 95
    upper_stress_bound: 97
piano:
    0:
    - Fs4 2
    - Cs4 8
    - A3 8
    2:
    - Gs4 1
    3:
    - A4 1
    4:
    - Fs4 4

---
# Measure 25
piano:
    0:
    - Fs4 2
    - Fs3 4
    - B2 4
    2:
    - Ds4 1
    3:
    - Fs4 1
    4:
    - B4 2
    - Fs4 2
    - Ds3 2
    6:
    - Gs4 6
    - E4 6
    - Cs4 6
    - Gs3 6

---
# Measure 26
piano:
    4:
    - Gs4 2
    - B3 1
    - E3 4
    5:
    - E4 1
    6:
    - E4 1
    7:
    - Cs4 1

---
# Measure 27
piano:
    0:
    - A4 6
    - E4 6
    - Cs4 6
    - Cs3 6
    2:
    - A4 4
    3:
    - E4 3
    4:
    - Fs4 2
    - Ds4 2
    - B3 2
    - Ds3 4
    6:
    - Fs4 2

---
# Measure 28
piano:
    0:
    - E4 4
    - B3 4
    - Gs3 2
    2:
    - E3 1
    3:
    - Cs3 1
    4:
    - E4 1
    - Cs4 1
    - A2 4
    5:
    - A4 1
    6:
    - Cs5 1
    7:
    - A4 1

---
# Measure 29
piano:
    0:
    - A4 2
    - Fs4 2
    - D4 2
    - D3 4
    2:
    - Gs4 1
    3:
    - Fs4 1
    4:
    - E4 2
    - B3 2
    - Gs3 4
    6:
    - Gs4 2

---
# Measure 30
piano:
    0:
    - A4 2
    - E4 2
    - Cs4 2
    - A3 4
    2:
    - E4 1
    3:
    - Cs4 1
    4:
    - Fs4 2
    - D4 2
    - B3 4
    6:
    - Fs4 1
    7:
    - B4 1

---
# Measure 31
piano:
    0:
    - E5 2
    - B4 2
    - Gs4 2
    - E3 2
    2:
    - B4 1
    3:
    - Gs4 1
    4:
    - Cs5 2
    - E4 2
    - A3 2
    6:
    - Cs4 2

---
# Measure 32
piano:
    0:
    - Cs5 2
    - A4 2
    - E4 2
    - E3 8
    2:
    - A4 1
    3:
    - E4 1
    4:
    - A4 2
    - Cs4 2
    6:
    - A4 1
    7:
    - Cs5 1

---
# Measure 33
piano:
    0:
    - E5 2
    - Cs5 2
    - A4 2
    - E3 4
    2:
    - Cs5 1
    3:
    - A4 2
    4:
    - Fs4 1
    - A3 2
    5:
    - Cs4 1
    6:
    - Fs4 1
    - Cs4 1
    7:
    - Gs4 1

---
# Measure 34
piano:
    0:
    - A4 2
    - Fs4 4
    - D4 4
    2:
    - D5 4
    4:
    - Fs4 4
    - A3 4
    6:
    - A4 2

---
# Measure 35
piano:
    0:
    - B4 2
    - E4 4
    - B3 4
    2:
    - E5 2
    4:
    - Gs5 1
    - E5 1
    - Gs4 4
    5:
    - D5 1
    6:
    - E5 1
    - B4 1
    7:
    - A4 1

---
# Measure 36
piano:
    0:
    - E4 2
    - Cs4 2
    - A3 4
    2:
    - D4 1
    3:
    - Cs4 1
    4:
    - E4 1
    - A3 1
    - Cs3 4
    5:
    - Cs4 1
    6:
    - D4 1
    7:
    - Cs4 1

---
# Measure 37
piano:
    0:
    - A3 2
    - Cs3 4
    - Cs2 4
    2:
    - Gs4 1
    3:
    - A4 1
    4:
    - Cs5 2
    - A3 4
    - E3 4
    6:
    - E5 2

---
# Measure 38
piano:
    0:
    - Fs5 2
    - D5 2
    - A3 4
    2:
    - D5 1
    3:
    - Cs5 1
    4:
    - A4 4
    - D4 2
    - A3 2
    6:
    - Cs4 1
    7:
    - A3 1

---
# Measure 39
piano:
    0:
    - Ds5 2
    - A4 2
    - Fs3 4
    2:
    - Cs5 1
    3:
    - B4 1
    4:
    - Gs4 1
    - E3 4
    - B2 4
    5:
    - E4 1
    6:
    - A4 1
    7:
    - B4 1

---
# Measure 40
piano:
    0:
    - Cs5 2
    - A4 2
    - E4 8
    - Cs4 8
    2:
    - Cs5 1
    3:
    - E5 1
    4:
    - Cs5 4

---
# Measure 41
piano:
    0:
    - E4 10
    - B3 10
    - Gs3 10
    2:
    - E4 8
    3:
    - Gs4 7
    4:
    - E5 6
    - E4 6

---
# Measure 42
piano:
    0:
    - Cs5 2
    - A4 2
    - E4 4
    - A3 4
    2:
    - Cs5 1
    3:
    - E4 1
    4:
    - A3 4
    5:
    - Cs5 1
    6:
    - B4 1
    7:
    - A4 1

---
# Measure 43
piano:
    0:
    - B4 2
    - Fs4 2
    - D4 2
    - B3 4
    2:
    - A4 1
    3:
    - Fs4 1
    4:
    - E4 2
    - E3 4
    6:
    - Gs4 2

---
# Measure 44
piano:
    0:
    - Ds4 2
    - B3 4
    - Fs3 4
    2:
    - Cs4 2
    4:
    - Ds4 2
    - Fs3 4
    6:
    - Fs4 1
    7:
    - As4 1

---
# Measure 45
piano:
    0:
    - B4 4
    - Fs4 4
    - Ds4 4
    - Ds3 8
    5:
    - B4 1
    6:
    - Ds5 1
    7:
    - B5 1

---
# Measure 46
piano:
    0:
    - Gs5 2
    - E5 2
    - E3 4
    2:
    - B4 2
    4:
    - E5 2
    - Gs4 2
    - B3 4
    - Gs3 4
    6:
    - B4 2

---
# Measure 47
piano:
    0:
    - Fs5 2
    - Cs5 2
    - As4 2
    - Fs3 4
    2:
    - Gs4 1
    3:
    - As4 1
    4:
    - As4 2
    - Fs4 2
    - As3 4
    - Cs3 4
    6:
    - Cs5 2

---
# Measure 48
piano:
    0:
    - B4 3
    - Fs4 2
    - Ds4 2
    2:
    - Cs4 2
    3:
    - Fs4 1
    4:
    - Ds4 1
    - B3 4
    5:
    - B4 1
    - Fs4 1
    6:
    - Gs4 1
    7:
    - As4 1

---
# Measure 49
piano:
    0:
    - B4 3
    - Gs4 2
    - E4 2
    2:
    - E3 2
    3:
    - Gs4 1
    4:
    - Fs4 1
    - B2 2
    5:
    - Ds4 1
    6:
    - A4 1
    - C4 2
    - E3 2
    7:
    - C5 1

---
# Measure 50
piano:
    0:
    - B4 3
    - G4 3
    - E4 3
    2:
    - E3 2
    4:
    - B3 4
    - G3 4
    5: E4 3
    6: G4 2
    7: E4 1

---
# Measure 51
piano:
    0: # Dp
    - B4 6
    - G4 6
    - D4 6
    - G3 6
    2: B4 4
    3: D5 3
    4: # T
    - G5 2
    - D5 2
    - B4 2
    - D4 4
    6: B4 2

---
# Measure 52
piano:
    0: # S
       - C5 5
       - A4 5
       - E4 5
       - A3 5
    2: A4 3
    3: A4 2
    4:
       - A4 1
       - C4 4
       - C3 4
    5:
       - E4 1
       - C4 1
    6: Fs4 1
    7: E4 1

---
# Measure 53
piano:
    0: # T
       - E4 6
       - B3 6
       - G3 6
       - E3 6
    2: B3 4
    4:
       - G4 2
       - E4 2
       - B3 4
       - B2 4
    6: B4 2

---
# Measure 54
piano:
    0: # Tp
       - C4 6
       - G3 6
       - C3 6
    2: E4 4
    4:
       - G4 2
       - C4 2
       - E3 4
    6: C5 2
    7: E4 1

---
# Measure 55
piano:
    0: # S
       - C5 2
       - A4 1.5
       - E4 1.5
       - A3 8
    2:
       - A4 0.75
       - E4 1
    3: A4 1
    4:
       - C4 1
       - Fs3 1
    5:
       - E4 1
       - C4 1
    6: Fs4 1
    7: E4 1

---
# Measure 56
piano:
    0:
       - C5 2
       - A4 2
       - E4 3
       - A2 8
    2: C4 1
    3: A4 1
    4: C5 1
    5:
       - A4 1
       - C4 1
    6: Fs4 1
    7: E4 1

---
# Measure 57
_meta:
    lower_stress_bound: 95;1,95;2,97;4,97
    upper_stress_bound: 97;1,98;2,100;4,100
piano:
    0: # D
       - Fs4 8
       - D4 8
       - B3 8
       - Fs3 8
    2:
       - B4 6
       - Fs4 6
    4: # T
       - G4 4
       - E4 4
       - B3 4
       - E3 12

---
# Measure 58
_meta:
    lower_stress_bound: 97
    upper_stress_bound: 100
piano:
    0:
    - G4 2
    - G3 8
    2: E3 2
    4:
    - E4 2
    - C4 2
    - A3 2
    6: D4 1
    7:
    - B4 1
    - Fs4 1
---
# Measure 59
piano:
    0:
    - E4 8
    - B3 8
    - G3 8
