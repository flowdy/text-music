title: Auf dem Wasser zu singen
composer: Franz Schubert
source: 'Franz Schubert – 100 Songs. High voice. Hal Leonard Corp. : Milwaukee, 2000.'

stage:
   high:
       direction: 1|1
       distance: 0
       instrument: kindopia
       articles:
         L: { FM: "6[4,1;5,0];1:35" }
         a: { weaken: "4;1,5;2,1" }
   middle: 2|3 1 xylophone
   low:
       direction: 3|2
       distance: 3
       instrument: sorgan
       articles:
           o: { weight: 3 }
           a: { weight: 5 }
           A: { weight: 7 }

instrument sorgan:
   NOT_CHANGED_SINCE: 2020-08-03 21:19:49
   FM: 6.317f[1,1;2,0];1:21
   TIMBRE: 10000:100;1,93;2,103!;3,99;4,91;6,98!;8,87;11,93!;13,85
   PROFILE:
   - V: 100
     A: 0.05:1,70;2,76;4,82;8,88;16,94;32,100;40,100
     S: 5:100;3,100;4,85
     R: 0.4:100;1,72;2,93;3,95;4,40;5,0
   - V: 95
     A: 0.04:1,1
     R: 0.45:1;1,1;2,0
   - [92, 87, 83]
   - V: 77
     A: 0.05:1,1;2,1;4,1
     S: 4:100;1,83;7,77
     R: 0.5:1;1,0
   MORPH:
   - 1 1;1,1
   - 6 6;3,7;9,4
   - 2n 6;1,7;2,7

instrument xylophone:
   NOT_CHANGED_SINCE: 2020-08-03 20:15:12
   A: 0.03:1,10;2,30;3,30;4,27
   S: 0.4:36;1,36;2,34;8,33
   R: 0.5:1;1,1;2,0
   AM: 3[1,1];1:3
   FM: 5.31f[1;1,0;3,0];4:1

instrument kindopia:
    - # NOT_CHANGED_SINCE: 2020-08-01 15:33:29
      R: 1:10;3,4;4,0
      TIMBRE: 15000:100;3,86;5,92;6,85;8,69;9,65;12,100;17,94
      SPREAD: 30:-5+10;0,4;3,1;4,6;5,3;7,9;8,7;12,0
      VOLUMES: 30:100;1,85;2,95;5,70;30,0
      PROFILE:
      - A: 0.025:1,1;3,1;5,1
        S: 4:100;1,98;2,101;3,90;4,85;5,85;6,90;25,84
      - S: 4:100;1,80;2,77!;3,82;4,85;5,90;8,80;40,60
      - match: 4
        S: 4:80;1,40;2,45!;40,40
      - match: 8n
        V: -40
      - match: 10
        A: 0.02:1,1;4,1
      - match: 30
        A: 0.01:1,1
        S: 2:10;3,4;4,0
    - ATTR: weaken
      1:
        MORPH:
        - 1 4;0,5;1,5;2,4
        - 10 1;1,0
      5:
        MORPH:
        - 1 1;1,1
        - 10 1;1,1

# Abm Ab Bb Cb Db Eb Fb Gb Ab
# Ab  Ab Bb C  Db Eb F  G  Ab

---
_meta:
  stress_pattern: 2,0,0,1,0,0;1,0,0,0
  ticks_per_minute: 490
  upper_stress_bound: 70;1,75;2,70
  lower_stress_bound: 66
middle: Eb4 o_     o+12. o_o--.o_o--. o_o-.o_o--.o_o-5.
low:
      - Cb4 A-14_3 o=_3 a=_3 o=_3 A=_3       o_3
      - Ab3 .4     a_3 o_3 A_3 o_3            a_3
      - Eb3 .4     o_3 A_3 o_3 a_3            o_3

---
_meta:
  repeat_unmentioned_voices: True

---
middle: Eb4 o_o+13. o_o-. o_o--. o_o--. o_o-. o_o-7.
low:
      - Db4 A-14_3 o=_3a=_3o=_3A=_3a_3
      - G3  .4     o=_3a=_3o=_3A=_3o_3
      - Eb3 .4     o=_3o=_3A=_3o=_3 o_3

---
_meta:
  repeat_unmentioned_voices: True

---
middle: Eb4 o_   o+12. o_o--.o_o--. o_o-.o_o--.o_o-5_
low:
      - Cb4 o-14_3 o=_3 o=_3 o=_3 o=_3       o_3
      - Ab3 .4    o_3 o_3 o_3 o_3            o_3
      - Eb3 .4    o_3 o_3 o_3 o_3            o_3

---
_meta:
  upper_stress_bound: 77;1,70;2,75;4,70
middle: Ab4 oo+12 o--. o_o--. o_o-. o_o-. o_o+. o_o++.
low:
  - Cb4 o-19_3 o=_3A_3o_3a_3 A_3
  - Ab3 .4     o=_3a_3A_3o_3 a_3
  - Fb3 .4     A=_3o_3a_3A_3 o_3

---
_meta:
  upper_stress_bound: 70
middle:
  0:
  - Fb5 2 1+3
  - Eb5 .. o=_o--_o--_o--_o-_..o+5_o--_o--_o--_o-_
  12:                        Eb5 2 1+3
low:
  0:
  - Cb4 o-20_3 A=_3 o_3
  - Ab3 .4     o=_3 A_3
  - Eb3 .4     a=_3 o_3
  12:
  - Db4 o-22_3 A=_3 o_3
  - Bb3 .4     a=_3 a_3
  - Eb3 .4     o=_3 A_3

---
middle: Bb4 o_o--_o-5_o-4_o+4_o+4_o+_3
low:
      - Cb4 o-14_3 a=_3 o_3 a_3 o_3 o_3
      - Ab3 .4     o=_3 A_3 o_3 a_3 o_3
      - Eb3 .4     a=_3 o_3 a_3 o_3 A_3

---
# 1. Strophe
_meta:
  upper_stress_bound: 70
  lower_stress_bound: 66
high:   Eb4 L_5          o+_o-_2.     a_3o+5_3o=_2.
middle: Eb4 o_     o+12. o_o--.o_o--. o_o-.o_o--.o_o-5.
low:
      - Cb4 o-14_3 o=_3 A_3 o_3 o_3 o_3
      - Ab3 .4     A=_3 o_3 a_3 A_3 o_3
      - Eb3 .4     a=_3 a_3 A_3 o_3 a_3

---
_meta:
  repeat_unmentioned_voices: True
high:   Eb4 a_o+8.o_o-.o_o--. L_7 o=_3

---
high:   Eb4 L_5          o+_o-_2.     a_3o+7_3o=_2.
middle: Eb4 o_o+13. o_o-. o_o--. o_o--. o_o-. o_o-7.
low:
      - Db4 o-14_3 o=_3 A_3 a_3 o_3 A_3
      - G3  .4     a=_3 o_3 o_3 a_3 a_3
      - Eb3 .4     o=_3 a_3 o_3 A_3 o_3

---
_meta:
  repeat_unmentioned_voices: True
high:   Eb4 a_o+10.o_o--.o_o-. L_11

---
_meta:
  upper_stress_bound: 74;1,76;2,74
  lower_stress_bound: 70;1,73;2,70
high: Cb5 a_o+4. o_o--. o_o--. L_5 o-5.o_2.
middle: Gb4 o_o+12.o_o--.o_o-.o_o--.o_o--.o_o-5_
low:
- Eb4 o-16_3 o=_3 A_3 o_3 o_3 a_3
- Cb4 .4     A=_3 o_3 a_3 a_3 o_3
- Gb3 .4     o=_3 a_3 A_3 o_3 A_3

---
high: Gb4 a_o+10.o_o-.o_o--. L_7 o-7_3
middle: Gb4 o_o+14.o_o--.o_o--.o_o-.o_o--.o_o-5_
low:
- Fb4 o-22_3 A=_3 a_3 o_3 o_3 a_3
- Bb3 .4     o_3  A_3 o_3 A_3 o_3
- Gb3 .4     a_3  o_3 a_3 o_3 a_3

---
_meta:
  upper_stress_bound: 74-76
  lower_stress_bound: 70-71
high: Eb5 L_5o.o_2. a_5o-4_o-3_3
middle: Gb4 o_o+12.o_o-3.o_o-4_ o-3_o+12.o_o-5.o_o-4_
low:
- Eb4 o-16_3 A=_3o_3 o-19_3 a=_3o_3
- Cb4 .4     o=_3A_3 .4     A=_3a_3
- Gb3 .4     a=_3o_3 .4     o=_3o_3

---
_meta:
  upper_stress_bound: 76-79
  lower_stress_bound: 71-75
high: Bb4 o_3a+5_3o-8_3 L+_11
middle: Bb4 o_o+12.o_o-3.o_o-4_ o-7_o+12.o_o-5.o_o-4_
low:
- Eb4 o-12_3 a=_3o_3 o-19_3 A=_3 o_3
- Db4 .4     o=_3a_3 .4     a--_3A_3
- Bb3 .4     A=_3o_3 .4     o--_3o_3

---
_meta:
  upper_stress_bound: 72;1,74;2,72
  lower_stress_bound: 68;1,71;2,68
high: Gb4 a_o+9.o_o--.o_o--. L_5o-5.o_3
middle: Gb4 o_o+12.o_o--.o_o-. o_o--.o_o--.o_o-5.
low:
- Eb4 o-16_3 a=_3 o_3 a_3 o_3 A_3
- Cb4 .4     o=_3 a_3 o_3 A_3 o_3
- Gb3 .4     a=_3 A_3 o_3 a_3 a_3

---
high: Gb4 a_o+10.o_o-.o_o--.L_7o-7_3
middle: Gb4 o_o+14.o_o--.o_o--. o_o-.o_o--.o_o-7.
low:
- Fb4 o-22_3 A=_3 a_3 o_3 a_3 o_3
- Bb3 .4     o=_3 A_3 a_3 a_3 o_3
- Gb3 .4     a=_3 o_3 o_3 o_3 A_3

---
_meta:
  upper_stress_bound: 72-74
  lower_stress_bound: 68-69
high: Eb5 L_5o.o_2. L_5o-4_o-3_3
middle: Gb4 o_o+12.o_o-3.o_o-4_ o-3_o+12.o_o-5.o_o-4_
low:
- Eb4 o-16_3 o=_3A_3 a-19_3 o=_3a_3
- Cb4 .4     a=_3o_3 .4     o=_3A_3
- Gb3 .4     A=_3a_3 .4     a=_3o_3

---
_meta:
  upper_stress_bound: 74-77
  lower_stress_bound: 69-73
high: Bb4 o_3a+5_3o-8_3 L+_11
middle: Bb4 o_o+12.o_o-3.o_o-4_ o-7_o+12.o_o-5.o_o-4_
low:
- Eb4 o-12_3 a=_3A_3 o-19_3 a=_3 o_3
- Db4 .4     a=_3o_3 .4     A--_3o_3
- Bb3 .4     o=_3o_3 .4     o--_3a_3

---
_meta:
  upper_stress_bound: 72;1,74;2,72
  lower_stress_bound: 68;1,71;2,68
middle: A4 o_o+9.o_o--.o_o-. o_o-.o_o+.o_o-6.
low:
- Gb4 o-18_3 A=_3 a_3 o_3 A_3 a_3
- Eb4 .4     o=_3 A_3 o_3 o_3 A_3
- A3  .4     a=_3 o_3 A_3 a_3 o_3

---
high: Eb5 a_5o+_o=_3 L+3_5o--_o-_3
middle: A4 o_o+9.o_o--.o_o-. o_o-.o_o+.o_o-6.
low:
- Gb4 o-18_3 A=_3 o_3 a_3 o_3 A_3
- Eb4 .4     o=_3 A_3 o_3 a_3 o_3
- A3  .4     a=_3 o_3 A_3 o_3 a_3

---
_meta:
  upper_stress_bound: 72
  lower_stress_bound: 68
high: Eb5 a_5o--_o=_3 L+_7 o-3_3
middle: A4 o_o+9.o_o--.o_o-. o-7_o+8.o_o-.o_o--.
low:
- Gb4 o-18_3 A=_3o_3 o-16_3 a+14_3o_3
- Eb4 .4     a=_3a_3 .4     A--_3 a_3
- A3  .4     o=_3o_3 .4     o-_3  a_3

---
high: Db5 a_5o-5.o_3 L+8_5o-_o--_2.
middle: Ab4 o_o+8.o_o-.o_o--_ o=_o+12.o_o--.o_o--_
low:
  dbm:
  - Fb4 o-15_3 o=_3o_3
  - Db4 .4     A=_3a_3
  - Ab3 .4     a=_3o_3
  0: <dbm
  12: <dbm

---
high: Db5 a_5o--_o-_3 L--_11
middle: Eb4 o_o+13.o_o-.o_o--_ o=_o+8.o_o--.o_o--_
low:
  0:
  - Db4 o-22_3 A=_3a_3
  - G3  .4     o=_3o_3
  - Eb3 .4     a=_3A_3
  12:
  - Cb4 o-15_3 o=_3o_3
  - Ab3 .4     A=_3a_3
  - Eb3 .4     o=_3o_3

---
high:
  _meta:
    upper_stress_bound: 72-77
    lower_stress_bound: 68-72
  0:
    pitch: Eb5
    length: 42 # 24 + 18
    shaped_stress: 5;3,7;4,6
    chain: L
middle: Eb4 o_o+10.o_o--.o_o--_ o=_o+8.o_o--.o_o--_
low:
  0:
  - Db4 o-22_3 o=_3o_3
  - G3  .4     A=_3a_3
  - Eb3 .4     o=_3o_3
  12:
  - Cb4 o-15_3 a=_3o_3
  - Ab3  .4    o=_3A_3
  - Eb3  .4    A=_3o_3

---
_meta:
  upper_stress_bound: 72;1,77;2,73
  upper_stress_bound: 68;1,73;2,70
high:
  _meta:
    upper_stress_bound: 77-82
    lower_stress_bound: 72-77
  18: Db5 o_o-_3
middle: Eb4 o_o+10.o_o-.o_o--_ o=_o+9.o_o--.o_o--_
low:
  0:
  - Db4 o-22_3 a=_3A_3
  - G3  .4     o=_3o_3
  - Eb3 .4     A=_3a_3
  12:
  - C4  o-16_3 o=_3A_3
  - Ab3 .4     a=_3o_3
  - Eb3 .4     o=_3a_3

---
_meta:
  upper_stress_bound: 82
  lower_stress_bound: 77
high: F5 L_5o--_o--_2. a_5o-_o--_3
middle: F4 o_o+8.o_o-.o_o--. o-3_o+10.o_o--.o_o--_
low:
  - Db4 o-24_3 a=_3o_3 o-22_3 o=_2 .A_3
  - Bb3 .4     o=_3a_3 .4     a-3_3 o_3
  - F3  .4     A=_3o_3 .4     A--_3 o_3

---
_meta:
   upper_stress_bound: 79;1,75;2,72;3,79!;4,75;5,72
   lower_stress_bound: 74;1,70;2,68;3,74!;4,70;5,68
high: Ab4 8 L
middle:
  _articles:
     a: { adj_stress: +3 }
     l: { weight: 2 }
     o: { weight: 3 }
  0:
  - C5  a_o+12. o_o--.o_o--_ a-5_o+5.o_o-.o_o--_
  - Eb5 .4      o_2.o_2.     a_2.l_2.  l_3
  - C5  a_2.    o_2.o_2.     a_2.o_2.  o_3
low:
  _articles:
     a: { adj_stress: +3 }
  0:
  - C4  a-15_3 o=_3o_3 a_3o_3o_3
  - Ab3 .4     o=_3o_3 a_3o_3o_3
  - Eb3 .4     o=_3o_3 a_3o_3o_3

---
_meta:
  upper_stress_bound: 72;1,76;2,72
  lower_stress_bound: 68;1,72;2,72
middle:
  - Eb5 o_o++.o_o--.o_o++. o_o--.o_o++.o_o--.
  - Db5 o_o-7.o_o+7.o_o-7. o_o+7.o_o-7.o_o+7.
low:
  - Db4 o-22_3 A=_3 o_3 a_3 o_3 a_3
  - G3  .4     o=_3 a_3 o_3 A_3 o_3
  - Eb3 .4     a=_3 o_3 A_3 o_3 a_3

---
_meta:
   upper_stress_bound: 79;1,72;2,72;3,79!;4,75;5,72
   lower_stress_bound: 74;1,68;2,68;3,74!;4,70;5,68
middle:
  _articles:
     a: { adj_stress: +3 }
  0:
  - Eb5 a_o+9.  o_o--.o_o--_ a-5_o+5.o_o-.o_o--_
  - Eb5 a_2.    o_2.o_2.     a_2.o_2.  o_3
  - C5  a_2.    o_2.o_2.     a_2.o_2.  o_3
low:
  _articles:
     a: { adj_stress: +3 }
  0:
  - C4  a-15_3 o=_3A_3 a_3o_3A_3
  - Ab3 .4     A=_3o_3 a_3A_3o_3
  - Eb3 .4     o=_3o_3 a_3o_3o_3

---
_meta:
  upper_stress_bound: 72;1,76;2,72
  lower_stress_bound: 68;1,72;2,72
middle:
  - Eb5 o_o++.o_o--.o_o++. o_o--.o_o++.o_o--.
  - Db5 o_o-7.o_o+7.o_o-7. o_o+7.o_o-7.o_o+7.
low:
  - Db4 o-22_3 A=_3 o_3 a_3 o_3 A_3
  - G3  .4     o=_3 a_3 o_3 A_3 a_3
  - Eb3 .4     a=_3 o_3 A_3 o_3 o_3

---
_meta:
   upper_stress_bound: 72;2,72;3,71
   lower_stress_bound: 68;2,68;3,67
middle:
   - Ab4 o_o+4.o_o-4.o_o+4. o_o-4.o_o+4.o_o-4.
   - Eb4 o_2.*6
   - C4  o_2.*5 o_3
low:
   - Ab3 o-12_3 A=_3 o_3 a_3 o_3 o_3
   - Eb3 .4     o=_3 a_3 o_3 A_3 a_3

---
_meta:
   upper_stress_bound: 71-70
   lower_stress_bound: 68-66
middle:
   - Ab4 o_o+3.o_o-3.o_o+3. o_o-3.o_o+3.o_o-3.
   - Eb4 o_2.*6
   - Cb4 o_2.*5 o_3
low:
   - Ab3 o-12_3 o=_3 A_3 o_3 a_3 o_3
   - Eb3 .4     a=_3 o_3 A_3 o_3 a_3

---
# 2. Strophe
_meta:
  upper_stress_bound: 70
  lower_stress_bound: 66
high:   Eb4 L_5          o+_o-_2.     a_3o+5_3o=_2.
middle: Eb4 o_     o+12. o_o--.o_o--. o_o-.o_o--.o_o-5.
low:
      - Cb4 o-14_3 o=_3 A_3 o_3 o_3 o_3
      - Ab3 .4     A=_3 o_3 a_3 A_3 o_3
      - Eb3 .4     a=_3 a_3 A_3 o_3 a_3

---
_meta:
  repeat_unmentioned_voices: True
high:   Eb4 a_o+8.o_o-.o_o--. L_7 o=_3

---
high:   Eb4 L_5          o+_o-_2.     a_3o+7_3o=_2.
middle: Eb4 o_o+13. o_o-. o_o--. o_o--. o_o-. o_o-7.
low:
      - Db4 o-14_3 o=_3 A_3 a_3 o_3 A_3
      - G3  .4     a=_3 o_3 o_3 a_3 a_3
      - Eb3 .4     o=_3 a_3 o_3 A_3 o_3

---
_meta:
  repeat_unmentioned_voices: True
high:   Eb4 a_o+10.o_o--.o_o-. L_11

---
_meta:
  upper_stress_bound: 74;1,76;2,74
  lower_stress_bound: 70;1,73;2,70
high: Cb5 a_o+4. o_o--. o_o--. L_5 o-5.o_2.
middle: Gb4 o_o+12.o_o--.o_o-.o_o--.o_o--.o_o-5_
low:
- Eb4 o-16_3 o=_3 A_3 o_3 o_3 a_3
- Cb4 .4     A=_3 o_3 a_3 a_3 o_3
- Gb3 .4     o=_3 a_3 A_3 o_3 A_3

---
high: Gb4 a_o+10.o_o-.o_o--. L_7 o-7_3
middle: Gb4 o_o+14.o_o--.o_o--.o_o-.o_o--.o_o-5_
low:
- Fb4 o-22_3 A=_3 a_3 o_3 o_3 a_3
- Bb3 .4     o_3  A_3 o_3 A_3 o_3
- Gb3 .4     a_3  o_3 a_3 o_3 a_3

---
_meta:
  upper_stress_bound: 74-76
  lower_stress_bound: 70-71
high: Eb5 L_5o.o_2. a_5o-4_o-3_3
middle: Gb4 o_o+12.o_o-3.o_o-4_ o-3_o+12.o_o-5.o_o-4_
low:
- Eb4 o-16_3 A=_3o_3 o-19_3 a=_3o_3
- Cb4 .4     o=_3A_3 .4     A=_3a_3
- Gb3 .4     a=_3o_3 .4     o=_3o_3

---
_meta:
  upper_stress_bound: 76-79
  lower_stress_bound: 71-75
high: Bb4 o_3a+5_3o-8_3 L+_11
middle: Bb4 o_o+12.o_o-3.o_o-4_ o-7_o+12.o_o-5.o_o-4_
low:
- Eb4 o-12_3 a=_3o_3 o-19_3 A=_3 o_3
- Db4 .4     o=_3a_3 .4     a--_3A_3
- Bb3 .4     A=_3o_3 .4     o--_3o_3

---
_meta:
  upper_stress_bound: 72;1,74;2,72
  lower_stress_bound: 68;1,71;2,68
high: Gb4 a_o+9.o_o--.o_o--. L_5o-5.o_3
middle: Gb4 o_o+12.o_o--.o_o-. o_o--.o_o--.o_o-5.
low:
- Eb4 o-16_3 a=_3 o_3 a_3 o_3 A_3
- Cb4 .4     o=_3 a_3 o_3 A_3 o_3
- Gb3 .4     a=_3 A_3 o_3 a_3 a_3

---
high: Gb4 a_o+10.o_o-.o_o--.L_7o-7_3
middle: Gb4 o_o+14.o_o--.o_o--. o_o-.o_o--.o_o-7.
low:
- Fb4 o-22_3 A=_3 a_3 o_3 a_3 o_3
- Bb3 .4     o=_3 A_3 a_3 a_3 o_3
- Gb3 .4     a=_3 o_3 o_3 o_3 A_3

---
_meta:
  upper_stress_bound: 72-74
  lower_stress_bound: 68-69
high: Eb5 L_5o.o_2. L_5o-4_o-3_3
middle: Gb4 o_o+12.o_o-3.o_o-4_ o-3_o+12.o_o-5.o_o-4_
low:
- Eb4 o-16_3 o=_3A_3 a-19_3 o=_3a_3
- Cb4 .4     a=_3o_3 .4     o=_3A_3
- Gb3 .4     A=_3a_3 .4     a=_3o_3

---
_meta:
  upper_stress_bound: 74-77
  lower_stress_bound: 69-73
high: Bb4 o_3a+5_3o-8_3 L+_11
middle: Bb4 o_o+12.o_o-3.o_o-4_ o-7_o+12.o_o-5.o_o-4_
low:
- Eb4 o-12_3 a=_3A_3 o-19_3 a=_3 o_3
- Db4 .4     a=_3o_3 .4     A--_3o_3
- Bb3 .4     o=_3o_3 .4     o--_3a_3

---
_meta:
  upper_stress_bound: 72;1,74;2,72
  lower_stress_bound: 68;1,71;2,68
middle: A4 o_o+9.o_o--.o_o-. o_o-.o_o+.o_o-6.
low:
- Gb4 o-18_3 A=_3 a_3 o_3 A_3 a_3
- Eb4 .4     o=_3 A_3 o_3 o_3 A_3
- A3  .4     a=_3 o_3 A_3 a_3 o_3

---
high: Eb5 a_5o+_o=_3 L+3_5o--_o-_3
middle: A4 o_o+9.o_o--.o_o-. o_o-.o_o+.o_o-6.
low:
- Gb4 o-18_3 A=_3 o_3 a_3 o_3 A_3
- Eb4 .4     o=_3 A_3 o_3 a_3 o_3
- A3  .4     a=_3 o_3 A_3 o_3 a_3

---
_meta:
  upper_stress_bound: 72
  lower_stress_bound: 68
high: Eb5 a_5o--_o=_3 L+_7 o-3_3
middle: A4 o_o+9.o_o--.o_o-. o-7_o+8.o_o-.o_o--.
low:
- Gb4 o-18_3 A=_3o_3 o-16_3 a+14_3o_3
- Eb4 .4     a=_3a_3 .4     A--_3 a_3
- A3  .4     o=_3o_3 .4     o-_3  a_3

---
high: Db5 a_5o-5.o_3 L+8_5o-_o--_2.
middle: Ab4 o_o+8.o_o-.o_o--_ o=_o+12.o_o--.o_o--_
low:
  dbm:
  - Fb4 o-15_3 o=_3o_3
  - Db4 .4     A=_3a_3
  - Ab3 .4     a=_3o_3
  0: <dbm
  12: <dbm

---
high: Db5 a_5o--_o-_3 L--_11
middle: Eb4 o_o+13.o_o-.o_o--_ o=_o+8.o_o--.o_o--_
low:
  0:
  - Db4 o-22_3 A=_3a_3
  - G3  .4     o=_3o_3
  - Eb3 .4     a=_3A_3
  12:
  - Cb4 o-15_3 o=_3o_3
  - Ab3 .4     A=_3a_3
  - Eb3 .4     o=_3o_3

---
high:
  _meta:
    upper_stress_bound: 72-77
    lower_stress_bound: 68-72
  0:
    pitch: Eb5
    length: 42 # 24 + 18
    shaped_stress: 5;3,7;4,6
    chain: L
middle: Eb4 o_o+10.o_o--.o_o--_ o=_o+8.o_o--.o_o--_
low:
  0:
  - Db4 o-22_3 o=_3o_3
  - G3  .4     A=_3a_3
  - Eb3 .4     o=_3o_3
  12:
  - Cb4 o-15_3 a=_3o_3
  - Ab3  .4    o=_3A_3
  - Eb3  .4    A=_3o_3

---
_meta:
  upper_stress_bound: 72;1,77;2,73
  upper_stress_bound: 68;1,73;2,70
high:
  _meta:
    upper_stress_bound: 77-82
    lower_stress_bound: 72-77
  18: Db5 o_o-_3
middle: Eb4 o_o+10.o_o-.o_o--_ o=_o+9.o_o--.o_o--_
low:
  0:
  - Db4 o-22_3 a=_3A_3
  - G3  .4     o=_3o_3
  - Eb3 .4     A=_3a_3
  12:
  - C4  o-16_3 o=_3A_3
  - Ab3 .4     a=_3o_3
  - Eb3 .4     o=_3a_3

---
_meta:
  upper_stress_bound: 82
  lower_stress_bound: 77
high: F5 L_5o--_o--_2. a_5o-_o--_3
middle: F4 o_o+8.o_o-.o_o--. o-3_o+10.o_o--.o_o--_
low:
  - Db4 o-24_3 a=_3o_3 o-22_3 o=_2 .A_3
  - Bb3 .4     o=_3a_3 .4     a-3_3 o_3
  - F3  .4     A=_3o_3 .4     A--_3 o_3

---
_meta:
   upper_stress_bound: 79;1,75;2,72;3,79!;4,75;5,72
   lower_stress_bound: 74;1,70;2,68;3,74!;4,70;5,68
high: Ab4 8 L
middle:
  _articles:
     a: { adj_stress: +3 }
     l: { weight: 2 }
     o: { weight: 3 }
  0:
  - C5  a_o+12. o_o--.o_o--_ a-5_o+5.o_o-.o_o--_
  - Eb5 .4      o_2.o_2.     a_2.l_2.  l_3
  - C5  a_2.    o_2.o_2.     a_2.o_2.  o_3
low:
  _articles:
     a: { adj_stress: +3 }
  0:
  - C4  a-15_3 o=_3o_3 a_3o_3o_3
  - Ab3 .4     o=_3o_3 a_3o_3o_3
  - Eb3 .4     o=_3o_3 a_3o_3o_3

---
_meta:
  upper_stress_bound: 72;1,76;2,72
  lower_stress_bound: 68;1,72;2,72
middle:
  - Eb5 o_o++.o_o--.o_o++. o_o--.o_o++.o_o--.
  - Db5 o_o-7.o_o+7.o_o-7. o_o+7.o_o-7.o_o+7.
low:
  - Db4 o-22_3 A=_3 o_3 a_3 o_3 a_3
  - G3  .4     o=_3 a_3 o_3 A_3 o_3
  - Eb3 .4     a=_3 o_3 A_3 o_3 a_3

---
_meta:
   upper_stress_bound: 79;1,72;2,72;3,79!;4,75;5,72
   lower_stress_bound: 74;1,68;2,68;3,74!;4,70;5,68
middle:
  _articles:
     a: { adj_stress: +3 }
  0:
  - Eb5 a_o+9.  o_o--.o_o--_ a-5_o+5.o_o-.o_o--_
  - Eb5 a_2.    o_2.o_2.     a_2.o_2.  o_3
  - C5  a_2.    o_2.o_2.     a_2.o_2.  o_3
low:
  _articles:
     a: { adj_stress: +3 }
  0:
  - C4  a-15_3 o=_3A_3 a_3o_3A_3
  - Ab3 .4     A=_3o_3 a_3A_3o_3
  - Eb3 .4     o=_3o_3 a_3o_3o_3

---
_meta:
  upper_stress_bound: 72;1,76;2,72
  lower_stress_bound: 68;1,72;2,72
middle:
  - Eb5 o_o++.o_o--.o_o++. o_o--.o_o++.o_o--.
  - Db5 o_o-7.o_o+7.o_o-7. o_o+7.o_o-7.o_o+7.
low:
  - Db4 o-22_3 A=_3 o_3 a_3 o_3 A_3
  - G3  .4     o=_3 a_3 o_3 A_3 a_3
  - Eb3 .4     a=_3 o_3 A_3 o_3 o_3

---
_meta:
   upper_stress_bound: 72;2,72;3,71
   lower_stress_bound: 68;2,68;3,67
middle:
   - Ab4 o_o+4.o_o-4.o_o+4. o_o-4.o_o+4.o_o-4.
   - Eb4 o_2.*6
   - C4  o_2.*5 o_3
low:
   - Ab3 o-12_3 A=_3 o_3 a_3 o_3 o_3
   - Eb3 .4     o=_3 a_3 o_3 A_3 a_3

---
_meta:
   upper_stress_bound: 71-70
   lower_stress_bound: 68-66
middle:
   - Ab4 o_o+3.o_o-3.o_o+3. o_o-3.o_o+3.o_o-3.
   - Eb4 o_2.*6
   - Cb4 o_2.*5 o_3
low:
   - Ab3 o-12_3 o=_3 A_3 o_3 a_3 o_3
   - Eb3 .4     a=_3 o_3 A_3 o_3 a_3

---
# 3. Strophe
_meta:
  upper_stress_bound: 70
  lower_stress_bound: 66
high:   Eb4 L_5          o+_o-_2.     a_3o+5_3o=_2.
middle: Eb4 o_     o+12. o_o--.o_o--. o_o-.o_o--.o_o-5.
low:
      - Cb4 o-14_3 o=_3 A_3 o_3 o_3 o_3
      - Ab3 .4     A=_3 o_3 a_3 A_3 o_3
      - Eb3 .4     a=_3 a_3 A_3 o_3 a_3

---
_meta:
  repeat_unmentioned_voices: True
high:   Eb4 a_o+8.o_o-.o_o--. L_7 o=_3

---
high:   Eb4 L_5          o+_o-_2.     a_3o+7_3o=_2.
middle: Eb4 o_o+13. o_o-. o_o--. o_o--. o_o-. o_o-7.
low:
      - Db4 o-14_3 o=_3 A_3 a_3 o_3 A_3
      - G3  .4     a=_3 o_3 o_3 a_3 a_3
      - Eb3 .4     o=_3 a_3 o_3 A_3 o_3

---
_meta:
  repeat_unmentioned_voices: True
high:   Eb4 a_o+10.o_o--.o_o-. L_11

---
_meta:
  upper_stress_bound: 74;1,76;2,74
  lower_stress_bound: 70;1,73;2,70
high: Cb5 a_o+4. o_o--. o_o--. L_5 o-5.o_2.
middle: Gb4 o_o+12.o_o--.o_o-.o_o--.o_o--.o_o-5_
low:
- Eb4 o-16_3 o=_3 A_3 o_3 o_3 a_3
- Cb4 .4     A=_3 o_3 a_3 a_3 o_3
- Gb3 .4     o=_3 a_3 A_3 o_3 A_3

---
high: Gb4 a_o+10.o_o-.o_o--. L_7 o-7_3
middle: Gb4 o_o+14.o_o--.o_o--.o_o-.o_o--.o_o-5_
low:
- Fb4 o-22_3 A=_3 a_3 o_3 o_3 a_3
- Bb3 .4     o_3  A_3 o_3 A_3 o_3
- Gb3 .4     a_3  o_3 a_3 o_3 a_3

---
_meta:
  upper_stress_bound: 74-76
  lower_stress_bound: 70-71
high: Eb5 L_5o.o_2. a_5o-4_o-3_3
middle: Gb4 o_o+12.o_o-3.o_o-4_ o-3_o+12.o_o-5.o_o-4_
low:
- Eb4 o-16_3 A=_3o_3 o-19_3 a=_3o_3
- Cb4 .4     o=_3A_3 .4     A=_3a_3
- Gb3 .4     a=_3o_3 .4     o=_3o_3

---
_meta:
  upper_stress_bound: 76-79
  lower_stress_bound: 71-75
high: Bb4 o_3a+5_3o-8_3 L+_11
middle: Bb4 o_o+12.o_o-3.o_o-4_ o-7_o+12.o_o-5.o_o-4_
low:
- Eb4 o-12_3 a=_3o_3 o-19_3 A=_3 o_3
- Db4 .4     o=_3a_3 .4     a--_3A_3
- Bb3 .4     A=_3o_3 .4     o--_3o_3

---
_meta:
  upper_stress_bound: 72;1,74;2,72
  lower_stress_bound: 68;1,71;2,68
high: Gb4 a_o+9.o_o--.o_o--. L_5o-5.o_3
middle: Gb4 o_o+12.o_o--.o_o-. o_o--.o_o--.o_o-5.
low:
- Eb4 o-16_3 a=_3 o_3 a_3 o_3 A_3
- Cb4 .4     o=_3 a_3 o_3 A_3 o_3
- Gb3 .4     a=_3 A_3 o_3 a_3 a_3

---
high: Gb4 a_o+10.o_o-.o_o--.L_7o-7_3
middle: Gb4 o_o+14.o_o--.o_o--. o_o-.o_o--.o_o-7.
low:
- Fb4 o-22_3 A=_3 a_3 o_3 a_3 o_3
- Bb3 .4     o=_3 A_3 a_3 a_3 o_3
- Gb3 .4     a=_3 o_3 o_3 o_3 A_3

---
_meta:
  upper_stress_bound: 72-74
  lower_stress_bound: 68-69
high: Eb5 L_5o.o_2. L_5o-4_o-3_3
middle: Gb4 o_o+12.o_o-3.o_o-4_ o-3_o+12.o_o-5.o_o-4_
low:
- Eb4 o-16_3 o=_3A_3 a-19_3 o=_3a_3
- Cb4 .4     a=_3o_3 .4     o=_3A_3
- Gb3 .4     A=_3a_3 .4     a=_3o_3

---
_meta:
  upper_stress_bound: 74-77
  lower_stress_bound: 69-73
high: Bb4 o_3a+5_3o-8_3 L+_11
middle: Bb4 o_o+12.o_o-3.o_o-4_ o-7_o+12.o_o-5.o_o-4_
low:
- Eb4 o-12_3 a=_3A_3 o-19_3 a=_3 o_3
- Db4 .4     a=_3o_3 .4     A--_3o_3
- Bb3 .4     o=_3o_3 .4     o--_3a_3

---
_meta:
  upper_stress_bound: 72;1,74;2,72
  lower_stress_bound: 68;1,71;2,68
middle: A4 o_o+9.o_o--.o_o-. o_o-.o_o+.o_o-6.
low:
- Gb4 o-18_3 A=_3 a_3 o_3 A_3 a_3
- Eb4 .4     o=_3 A_3 o_3 o_3 A_3
- A3  .4     a=_3 o_3 A_3 a_3 o_3

---
high: Eb5 a_5o+_o=_3 L+3_5o--_o-_3
middle: A4 o_o+9.o_o--.o_o-. o_o-.o_o+.o_o-6.
low:
- Gb4 o-18_3 A=_3 o_3 a_3 o_3 A_3
- Eb4 .4     o=_3 A_3 o_3 a_3 o_3
- A3  .4     a=_3 o_3 A_3 o_3 a_3

---
_meta:
  upper_stress_bound: 72
  lower_stress_bound: 68
high: Eb5 a_5o--_o=_3 L+_7 o-3_3
middle: A4 o_o+9.o_o--.o_o-. o-7_o+8.o_o-.o_o--.
low:
- Gb4 o-18_3 A=_3o_3 o-16_3 a+14_3o_3
- Eb4 .4     a=_3a_3 .4     A--_3 a_3
- A3  .4     o=_3o_3 .4     o-_3  a_3

---
high: Db5 a_5o-5.o_3 L+8_5o-_o--_2.
middle: Ab4 o_o+8.o_o-.o_o--_ o=_o+12.o_o--.o_o--_
low:
  dbm:
  - Fb4 o-15_3 o=_3o_3
  - Db4 .4     A=_3a_3
  - Ab3 .4     a=_3o_3
  0: <dbm
  12: <dbm

---
high: Db5 a_5o--_o-_3 L--_11
middle: Eb4 o_o+13.o_o-.o_o--_ o=_o+8.o_o--.o_o--_
low:
  0:
  - Db4 o-22_3 A=_3a_3
  - G3  .4     o=_3o_3
  - Eb3 .4     a=_3A_3
  12:
  - Cb4 o-15_3 o=_3o_3
  - Ab3 .4     A=_3a_3
  - Eb3 .4     o=_3o_3

---
high:
  _meta:
    upper_stress_bound: 72-77
    lower_stress_bound: 68-72
  0:
    pitch: Eb5
    length: 42 # 24 + 18
    shaped_stress: 5;3,7;4,6
    chain: L
middle: Eb4 o_o+10.o_o--.o_o--_ o=_o+8.o_o--.o_o--_
low:
  0:
  - Db4 o-22_3 o=_3o_3
  - G3  .4     A=_3a_3
  - Eb3 .4     o=_3o_3
  12:
  - Cb4 o-15_3 a=_3o_3
  - Ab3  .4    o=_3A_3
  - Eb3  .4    A=_3o_3

---
_meta:
  upper_stress_bound: 72;1,77;2,73
  upper_stress_bound: 68;1,73;2,70
high:
  _meta:
    upper_stress_bound: 77-82
    lower_stress_bound: 72-77
  18: Db5 o_o-_3
middle: Eb4 o_o+10.o_o-.o_o--_ o=_o+9.o_o--.o_o--_
low:
  0:
  - Db4 o-22_3 a=_3A_3
  - G3  .4     o=_3o_3
  - Eb3 .4     A=_3a_3
  12:
  - C4  o-16_3 o=_3A_3
  - Ab3 .4     a=_3o_3
  - Eb3 .4     o=_3a_3

---
_meta:
  upper_stress_bound: 82
  lower_stress_bound: 77
high: F5 L_5o--_o--_2. a_5o-_o--_3
middle: F4 o_o+8.o_o-.o_o--. o-3_o+10.o_o--.o_o--_
low:
  - Db4 o-24_3 a=_3o_3 o-22_3 o=_2 .A_3
  - Bb3 .4     o=_3a_3 .4     a-3_3 o_3
  - F3  .4     A=_3o_3 .4     A--_3 o_3

---
_meta:
   upper_stress_bound: 79;1,75;2,72;3,79!;4,75;5,72
   lower_stress_bound: 74;1,70;2,68;3,74!;4,70;5,68
high: Ab4 8 L
middle:
  _articles:
     a: { adj_stress: +3 }
     l: { weight: 2 }
     o: { weight: 3 }
  0:
  - C5  a_o+12. o_o--.o_o--_ a-5_o+5.o_o-.o_o--_
  - Eb5 .4      o_2.o_2.     a_2.l_2.  l_3
  - C5  a_2.    o_2.o_2.     a_2.o_2.  o_3
low:
  _articles:
     a: { adj_stress: +3 }
  0:
  - C4  a-15_3 o=_3o_3 a_3o_3o_3
  - Ab3 .4     o=_3o_3 a_3o_3o_3
  - Eb3 .4     o=_3o_3 a_3o_3o_3

---
_meta:
  upper_stress_bound: 72;1,76;2,72
  lower_stress_bound: 68;1,72;2,72
middle:
  - Eb5 o_o++.o_o--.o_o++. o_o--.o_o++.o_o--.
  - Db5 o_o-7.o_o+7.o_o-7. o_o+7.o_o-7.o_o+7.
low:
  - Db4 o-22_3 A=_3 o_3 a_3 o_3 a_3
  - G3  .4     o=_3 a_3 o_3 A_3 o_3
  - Eb3 .4     a=_3 o_3 A_3 o_3 a_3

---
_meta:
   upper_stress_bound: 79;1,72;2,72;3,79!;4,75;5,72
   lower_stress_bound: 74;1,68;2,68;3,74!;4,70;5,68
middle:
  _articles:
     a: { adj_stress: +3 }
  0:
  - Eb5 a_o+9.  o_o--.o_o--_ a-5_o+5.o_o-.o_o--_
  - Eb5 a_2.    o_2.o_2.     a_2.o_2.  o_3
  - C5  a_2.    o_2.o_2.     a_2.o_2.  o_3
low:
  _articles:
     a: { adj_stress: +3 }
  0:
  - C4  a-15_3 o=_3A_3 a_3o_3A_3
  - Ab3 .4     A=_3o_3 a_3A_3o_3
  - Eb3 .4     o=_3o_3 a_3o_3o_3

---
_meta:
  upper_stress_bound: 72;1,76;2,72
  lower_stress_bound: 68;1,72;2,72
middle:
  - Eb5 o_o++.o_o--.o_o++. o_o--.o_o++.o_o--.
  - Db5 o_o-7.o_o+7.o_o-7. o_o+7.o_o-7.o_o+7.
low:
  - Db4 o-22_3 A=_3 o_3 a_3 o_3 A_3
  - G3  .4     o=_3 a_3 o_3 A_3 a_3
  - Eb3 .4     a=_3 o_3 A_3 o_3 o_3

---
_meta:
   upper_stress_bound: 72;2,72;3,71
   lower_stress_bound: 68;2,68;3,67
middle:
   - Ab4 o_o+4.o_o-4.o_o+4. o_o-4.o_o+4.o_o-4.
   - Eb4 o_2.*6
   - C4  o_2.*5 o_3
low:
   - Ab3 o-12_3 A=_3 o_3 a_3 o_3 o_3
   - Eb3 .4     o=_3 a_3 o_3 A_3 a_3

---
_meta:
   upper_stress_bound: 71-70
   lower_stress_bound: 68-66
middle:
   - Eb5 o_o+5.o_o-5.o_o+5. o_o-5.o_o+5.o_o-5.
   - C5  o_2.*5 o_2.
   - Ab4 o_2.*5 o_2.
low:
   - C4  .4     A=_3 a_3 o_3 a_3 a_2.
   - Ab3 o-12_3 o=_3 A_3 a_3 o_3 A_2.
   - Eb3 .4     a=_3 o_3 o_3 a_3 o_2.

---
_meta:
  ticks_per_minute: 200
middle:
  - { pitch: Ab5, length: 24, shaped_stress: '10;1,14;2,14' }
  - { pitch: C5,  length: 24, shaped_stress: '10;1,14;2,14' }
  - { pitch: Ab4, length: 24, shaped_stress: '10;1,14;2,14' }
low: [Ab3 24, Eb3 24, Ab2 24]
