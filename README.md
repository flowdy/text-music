Text music
==========

This repository contains music in text format that humans as well as
machines can read.

Humans can read it given they know a bit of music theory and understand that most
features represented by iconographical means in western Common Music Notation must
be translated into numbers and labels. Tone pitches are labelled with octave numbers,
whereas in Germany and several other nations names like "c'" for C4 are common.
The pitch labels can be looked up in lib/tones_euro_de+en.splt file in Sompyler.

Computers can read it and make according sound files when they have sompyler installed.
"Sompyler" is a synthesizer software I wrote, too. It reads instrument specifications
(that by the way are in text format as well) and a score and renders a sound file
from scratch. No MIDI devices or sound fonts are required, but a performant multicore
CPU and a plenty of memory is recommended, depending on how many measures are contained
and on how many instruments play.

Sompyler is implemented in Python 3 and can be downloaded or forked from Gitlab:
   https://gitlab.com/flowdy/sompyler


Statement of copyright and license
----------------------------------

As the repository owner I declare that all Sompyler score files contained in this
repository are either my own compositions or compositions in public domain.

You may render my music and use it in compliance with

   Creative Commons 4.0 Attribution-NoDerivation (CC by-nd 4.0) license.

These restrictions do not apply to the music by other composers that is in the
public domain. Always look at the composer line in the metadata at the top of
a file.

Quality
-------

Stand-alone Sompyler scores rely on the instruments contained in the official sompyler
distribution. Those instruments will sound to you more or less acceptable in quality,
but they could hardly sound any better than real instruments, which is at least not the
objective in the first place.

Sompyler scores with instruments are packed together in directories. These instruments
underly the same license except where noted otherwised.


Changes
-------

The contents of the Sompyler score files are kept in line with the expectations of
the latest stable Sompyler version. However, as long as Sompyler version is not counted,
it is considered pre-1.0, therefore unstable (alpha/beta). When you have not changed
a thing but Sompyler emits errors rendering the file in question, please refer to the git
logs of Sompyler code and scores repositories to bring them in line.
